
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title', 'Home Page') </title>
	
	
</head>
<body>
	<div class="flex-center position-ref full-height">
		<div class="content">
			@include('project.header')

			 @yield('project_contents')
			@include('project.footer')


		</div>

	</div>

</body>
</html>